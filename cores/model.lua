------------------------------------------------------------
-- Copyleft (Я) 2021-2023 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------
-- An example on how to register your own cores
-- This would create a maidroid always wandering
------------------------------------------------------------

local S = maidroid.translator

-- Core interface functions
local on_start, on_pause, on_resume, on_stop, on_step, is_tool

local wander =  maidroid.cores.wander

on_start = function(self)
	wander.on_start(self)
end

on_resume = function(self)
	wander.on_resume(self)
end

on_stop = function(self)
	wander.on_stop(self)
end

on_pause = function(self)
	wander.on_pause(self)
end

-- is_tool can be nil if the core is a behavior
-- should return true if stack activates this core
-- example: this core is triggered when receiving
-- a maidroid egg in inventory before the current
-- activate item
is_tool = function(stack)
	return stack:get_name() == "maidroid:maidroid_egg"
end

on_step = function(self, dtime, moveresult)
	if self.state == maidroid.states.WANDER then
		wander.on_step(self, dtime, moveresult)
	--elseif self.state == maidroid.states.ACT then
	--	What should be done
	end
end

-- Optional: hat properties
local hat
if maidroid.settings.hat then
	hat = {
		name = "hat_model",
		mesh = "maidroid_hat_model.obj",
		textures = { "maidroid_hat_model.png" },
		offset = { x=0, y=0, z=0 },
		rotation = { x=0, y=0, z=0 },
	}
end
-- Optional: hat properties

local doc = "set doc only if you want to see a doc for this core"

maidroid.register_core("model", {
	description	= S("model"), -- description shown in GUI and infos
	on_start	= on_start,
	on_stop		= on_stop,
	on_resume	= on_resume,
	on_pause	= on_pause,
	on_step		= on_step,
	is_tool		= is_tool,
	-- select_tool = nil, -- Optional: maidroid
	hat         = hat, -- Optional: register a hat
	doc = doc,
})
-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
