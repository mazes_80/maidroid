# Maidroid [![ContentDB](https://content.minetest.net/packages/mazes/maidroid_ng/shields/downloads/)](https://content.minetest.net/packages/mazes/maidroid_ng/)

<img src=misc/banner.png>

This mod is inspired littleMaidMob mod from another voxel game, it provides maid robots called "maidroid".

These robots automatize some daily in-game tasks depending on the items you give them.

## How to Use

### Usage

1. Create a maidroid egg <img style="background-color: grey;" src="textures/maidroid_maidroid_egg.png"> and use it in world to spawn a maid.
3. Tame maidroid punching it with a golden pie <img style="background-color: grey;" width="16" height="16" src="textures/maidroid_pie_inv.png"> or one gold block <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_block.png"> when pie mod is not installed
4. You can colorize them by punching dyes
<table><tr>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_white.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_grey.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_dark_grey.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_black.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_violet.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_blue.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_cyan.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_dark_green.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_green.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_yellow.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_brown.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_orange.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_magenta.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_pink.png"></td>
<tr></table>

5. They will sit down and take a pause if you punch them with activation item
    * Paper <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"> by default
    * Sugar <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/farming_sugar.png"> if [farming redo](https://codeberg.org/tenplus1/farming/) available
6. To activate open their inventory with right-click and put tool and stuff inside.

<table>
<tr>
<td>Basic</td><td></td><td>Maidroids follow owner or any player when wild, if this player have taming item in hand.<br>
<br>When they don't follow anyone they just wander
<br>Defaut if no other core selected</td>
</tr>
<tr>
<td>Farming</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/farming/textures/farming_tool_diamondhoe.png"><br>
<img style="background-color: grey;" src="https://github.com/t-affeldt/sickles/blob/master/textures/sickles_scythe_bronze.png?raw=true"></td>
<td>Any hoe or scythe selects this core.<br>
Maidroid will plant the seeds or make seed from plants if they can.<br>
Maidroid harvests mature plants.<br>
Surrounded their land with fences or xpanes, as they are not jumpable by farmers.<br>
</td>
</tr>

<tr>
<td>OCR</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_book_written.png"></td>
<td>Maidroid needs a written book written in their inventory.<br> If the bookname is main, program is read.
<pre><code>start: sleep 1
beep
jump 0.9
jmp start
</code></pre>
</td>
</tr>

<tr>
<td>Torcher</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_torch_on_floor.png"></br>
<img style="background-color: grey;" src="https://github.com/mt-mods/abritorch/blob/master/textures/abritorch_torch_on_floor_green.png?raw=true"></td>
<td>Maidroids with torches in inventory will follow a player, and put torch if it is dark.<br>
torches from [abritorch](https://github.com/mt-mods/abritorch) are scheduling this core.
</td>
</tr>
<tr>
<td>Stockbreeder</td>
<td>
<img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/bucket/textures/bucket.png">
<br>
<img style="background-color: grey;" width="16" height="16" src="https://raw.githubusercontent.com/runsy/petz/master/petz/textures/petz_shears.png">
</td>
<td>An empty bucket or shears select this core<br>
Stockbreeder will try to feed milkable animals, and then milk them<br>
If they have shears they will also try to shear sheep<br>
If they also have a sword in inventory they will try to manage poultries population<br>
If they have shovel in inventory they can collect poops<br>
</td>
</tr>

<tr>
<td>Waffler</td><td><img style="background-color: grey;" src="https://github.com/GreenXenith/waffles/blob/master/textures/waffles_waffle_block_side.png?raw=true"></td>
<td>Waffler are automatized waffle producer units, activated by putting one waffle block in maidroid inventory<br>
They can remove waffle, put batter, and launch cook on waffle makers.<br>
It is also planned that they can prepare batter from flour and water, prepare flour from cereal, and collect water from "sources"
</td>
</tr>

</table>

#### Flush items

Overflows may happen when picking up nearby items in world or collecting resources
Select items to flush in according tab of interface

##### Pipeworks

+ Add a teleport tube in your maidroid inventory
+ Select a valid channel name: it must exist and be private, just use the channel name
+ Flush via the dedicated button or wait overflow

##### Chests

When overflow, if a valid chest with enough room is located nearby, maidroid will flush it

### Recipes

<table>
<tr>
  <th scope="col">Maidroid Egg</th>
  <th scope="col">Golden Pie</th>
  <th scope="col">Capture rod</th>
  <th scope="col">Nametag</th>
  <th scope="col">Robbery Stick</th>
</tr>
<tr>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_maidroid_egg.png"></td>
  <td style="background-color: grey;" align="center"><img width="16" height="16" src="textures/maidroid_pie_inv.png"></td>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_tool_capture_rod.png"></td>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_tool_nametag.png"></td>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_robbery_stick.png"></td>
</tr>
</tr>
<tr>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_coal_block.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_block.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_coal_block.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="textures/maidroid_tool_nametag.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_bronze_block.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_lump.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_ingot.png"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/wool/textures/wool_blue.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_crystal.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_steel_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_steel_ingot.png"></td>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/wool/textures/wool_violet.png"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/farming/textures/farming_cotton.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_black.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_copper_ingot.png"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img style="background-color: grey;" width="16" height="16" src="textures/maidroid_pie_inv.png">/<img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_block.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_stick.png">/<img src="https://github.com/mt-mods/basic_materials/blob/master/textures/basic_materials_steel_bar.png?raw=true"></td>
        <td style="background-color: grey;"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_stick.png">/<img src="https://github.com/mt-mods/basic_materials/blob/master/textures/basic_materials_steel_bar.png?raw=true"></td>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"></td>
      </tr>
    </table>
  </td>
</tr>
</table>

## New features
* Automatic core selection
* Paintable with dyes
* Strong ownership for maidroids
    * You need to tame new maidroids
    * You can view any maidroid content
    * You can only take and put items in your maidroids
    * Maidroid privilege allows to
        * kill any maidroid
        * interact with any maidroids
        * set any maidroid owner with nametag
* Farming
    * Fields separator can be xpanes or fences
        * through fences maidroid were harvesting plants from nearby fields
        * only in old maidroid version, now it seems better
    * Support for all farming redo crops and plants
        * Craft seeds for many plants: garlic, melon, pepper, pineapple, pumpkin
            * Melon and pumpkin require cutting board in inventory
        * Pepper can be harvested in three states: <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/crops_pepper.png"> <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/crops_pepper_yellow.png"> <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/crops_pepper_red.png">
        * Melons <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/farming_melon_7.png"> and pumpkins <img style="background-color: grey;" src="https://codeberg.org/tenplus1/farming/raw/branch/master/textures/farming_pumpkin_8.png"> can now harvested
    * Support for cucina_vegana
    * Support for better_farming
    * Offline mode: droid continue to work even owner is offline
    * Scythes supported: maidroid using it will harvest up to five mature plants and replace them by crops.
* Protected areas support
* Internationalization
* Health
    * Status in info
    * Egg bar in maidroid menu
    * Can be healed
        * punching with tin lump <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png">
        * punching with mese fragment <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_crystal_fragment.png">
        * Or just put those items in inventory for auto-healing
        * Drops all inventory items and some base material on death
* Older version backward compatibility
* Nametag: give maidroid via right-click
* Support for pipeworks teleport tubes: overflow management
* Inventory
    * Sneak click
    * 3D model view
    * Tabs
        * Inventory: main menu used before tabs
        * Flush: visible to owner
            * define items to be flushed by droid
            * define teleport tube active channel: visible if inventory contains teleport tube
            * flush immediately: visible when teleport tube channel is set
        * Shop: visible in compatible jobs
            * owner can set goods to sell and prices
            * other people can only buy items from shop.
                * price items are took from your inventory but action is not visible
    * Enlight selected tool
* Cores are now included in mod:
    * behaviors are madatory
    * job cores may be disabled by setting
    * some job core may be disabled when dependency not loaded
* Holding taming item in inventory will allow a madroid to tame nearby untamed maidroids
* Support for pie:
    * maidroid golden pie: <img style="background-color: grey;" width="16" height="16" src="textures/maidroid_pie_inv.png">
    * use it to tame maidroids
    * never eat this metallic pie
* Robbery stick: <img src="textures/maidroid_robbery_stick.png">
    * Punch maids near their owner with this stick
    * Does not work in protected areas
* Hats: some core may wear hats
* Chests: basic support for overflow

### Settings
* `maidroid.tools.capture_rod`
    * boolean: allow to enable/disable capture rod
* `maidroid.hat`
    * boolean: allow to enable/disable hat
#### Cores
* `maidroid.farming`
    * boolean: enable/disable farmers
* `maidroid.stockbreeder`
    * boolean: enable/disable stockbreeders
* `maidroid.torcher`
    * boolean: enable/disable torchers
* `maidroid.ocr`
    * boolean: enable/disable programmable droids

## Migration guide
If you already installed an old version of maidroid

1. Before launching add this to your minetest.conf
    * `maidroid.compat = true`
2. Launch your world and ensure you activate every maidroid on the map or captured egg from inventories
3. When migration is finished edit your minetest.conf and remove maidroid.compat
4. For a new installs compat is set to false by default
5. If you used this version: <span style="color:red">You CAN NOT go back to older versions!</span>
6. Original tagicar version and forks were modpacks, if you get warnings:
    * remove `load_mod_maidroid_core` from world.mt
    * remove `load_mod_maidroid_tools` from world.mt

## Dependencies

### Optional mods to get loaded before maidroids

* dye: MTG: some recipe break without it
* [better_farming](https://github.com/AtlanteFr/BeterFarming)
* [cucina_vegana](https://github.com/acmgit/cucina_vegana)
* [ethereal](https://codeberg.org/tenplus1/ethereal)
* farming: [farming redo](https://codeberg.org/tenplus1/farming) is supported as farming mod
* [pie](https://codeberg.org/tenplus1/pie): Special cakes to tame droids



### Supported mods, may be mandatory for some features

* bucket: MTG: required to milk cows, and produce waffle batter
* default: MTG: many recipe break without it
* wool: MTG: many recipe break without it
* [pdisc](https://github.com/HybridDog/pdisc): required for "programmable" droids
* [pipeworks](https://github.com/mt-mods/pipeworks): required to teleport items
* [waffles](https://github.com/GreenXenith/waffles): required to produce: waffles

One of the following required for stockbreeding
* [animalia](https://github.com/ElCeejo/animalia)
* [mobs_animal](https://codeberg.org/tenplus1/mobs_animal)
* [petz](https://github.com/runsy/petz)

## API

You can register your own maidroids using: maidroid.register_core(name, def)

Have a look at [sample](cores/model.lua) to see what you must interface

Your .luacheckrc should contain

    globals = {
        "maidroid.animation",
        "maidroid.helpers",
        "maidroid.timers",
        "maidroid.states",
    }

    read_globals = {
        "maidroid",
    }

Except it no documentation is currently planned:

* you can have a look at other [cores](cores/) too
* ask to mazes_8[012345] via minetest irc channel

## Code enhancement
* Behaviors core: deported code from initial cores
    * Follow: still to enhance but bit better than before
    * Wander:
        * Try to go back to spawn point if too far from it
        * Randomly jump when wandering
    * Path: follow path after path_finding
    * More flexibility to extend or change behavior management
* This version is less cpu greedy than older ones
    * Less pathfinding pathes called
    * Wielded item do not leak and survive server restart, even they were cleaned at restart
    * Use more associative arrays for faster look up
    * New setting to skip steps

More features to come, look at [TODO](TODO)

## Requirements

- Minetest 5.3.0

## About older versions

This mod fork is based on **Bingfeng version** [Minetest Forum](https://forum.minetest.net/viewtopic.php?f=9&t=25523)
* [IFRFSX](https://github.com/IFRFSX/bingfeng-maidroid "Github")

    Merged patchset
* [thePlasm](https://github.com/thePlasm/maidroid "Github")
* [HybridDog](https://github.com/HybridDog/maidroid "Github")
* [Desour](https://github.com/Desour/maidroid "Github")

**Original tacigar version** [Minetest Forum](https://forum.minetest.net/viewtopic.php?f=9&t=14808)
* [tacigar's Origin Version](https://github.com/tacigar/maidroid "Github")

The source code of Maidroid is available under the [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) or later license.

The resouces included in Maidroid are available under the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or later license.
